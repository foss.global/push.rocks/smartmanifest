/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartmanifest',
  version: '2.0.2',
  description: 'a module for creating web app manifests'
}
